/**
 * Класс Main
 */
public class Main {

    public static void main(String... args) {
        Dog dog = new Dog();
        Fox fox = new Fox();
        Cat cat = new Cat();


        dog.printDisplay();
        cat.printDisplay();
        fox.printDisplay();

    }


}
