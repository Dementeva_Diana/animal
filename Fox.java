/**
 * класс Fox (лиса)
 */
public class Fox extends Animals {
    public Fox() {
        this("Лисенок", "Уруруру");
    }


    protected Fox(String name, String voice) {
        super(name, voice);
    }
}
