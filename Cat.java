/**
 * класс Cat (Кошка)
 */
public class Cat extends Animals {
    public Cat() {
        this("Кися", "Мур-Мур");
    }


    protected Cat(String name, String voice) {
        super(name, voice);
    }
}
